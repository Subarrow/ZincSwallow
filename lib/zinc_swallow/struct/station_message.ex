defmodule ZincSwallow.Struct.StationMessage do
  @moduledoc """
  Struct and utils for station message
  """

  @enforce_keys [
    :id,
    :severity,
    :category,
    :suppress,
    :stations,
    :message,
    :darwin_ts
  ]

  defstruct @enforce_keys

  defp stations(nil) do
    []
  end

  defp stations(stations) do
    stations |> Enum.map(fn %{crs: crs, _id: :"P:StationMessage/Station"} -> crs end)
  end

  defp parse_int!(str) do
    {n, ""} = :string.to_integer(str)
    n
  end

  @type t() :: %__MODULE__{
          id: binary(),
          severity: integer(),
          category: binary(),
          suppress: boolean(),
          stations: [binary()],
          message: binary(),
          darwin_ts: DateTime.t()
        }

  @spec from_root_xml(map()) :: t()
  def from_root_xml(%{
        version: "16.0",
        choice: %{
          OW: [
            %{
              id: id,
              cat: category,
              sev: severity,
              suppress: suppress_raw,
              Station: stations_raw,
              Msg: %{
                choice: message_raw
              },
              _id: :"P:StationMessage"
            }
          ]
        },
        ts: darwin_ts_raw
      }) do
    {:ok, darwin_ts, _} = DateTime.from_iso8601(darwin_ts_raw)

    %__MODULE__{
      id: Integer.to_string(id),
      category: category,
      severity: parse_int!(severity),
      suppress: suppress_raw || false,
      stations: stations(stations_raw),
      message: Enum.join(message_raw, ""),
      darwin_ts: darwin_ts
    }
  end
end

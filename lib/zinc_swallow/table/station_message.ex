defmodule ZincSwallow.Table.StationMessage do
  alias ZincSwallow.Struct

  @table_name :zincswallow_message_changelog

  def create_table() do
    :mnesia.create_table(@table_name,
      disc_copies: [node()],
      attributes: [:id, :current, :changelog, :darwin_ts, :is_cleared],
      index: [:darwin_ts, :is_cleared]
    )
  end

  def insert(%{
        id: id,
        current: current,
        changelog: changelog,
        darwin_ts: darwin_ts,
        is_cleared: is_cleared
      }) do
    :mnesia.write({@table_name, id, current, changelog, darwin_ts, is_cleared})
  end

  def insert_and_compare(
        current = %Struct.StationMessage{
          id: message_id,
          darwin_ts: current_darwin_ts,
          stations: stations
        }
      ) do
    case :mnesia.read(@table_name, message_id) do
      # Former and current are the same, down to the TS, so no change
      # On the other hand, if the new TS is lower than the old TS, then it's not actually new after all
      [{@table_name, _message_id, former, _former_changelog, former_darwin_ts, _is_cleared}]
      when former == current or current_darwin_ts < former_darwin_ts ->
        nil

      # Former and current are different
      [{@table_name, _message_id, former, former_changelog, _darwin_ts, _is_cleared}] ->
        new_changelog = [compare(former, current)] ++ former_changelog

        :ok =
          :mnesia.write(
            {@table_name, message_id, current, new_changelog, current_darwin_ts,
             length(stations) == 0}
          )

        {current, new_changelog}

      # There is no former, which means this is new
      [] ->
        :ok =
          :mnesia.write(
            {@table_name, message_id, current, [], current_darwin_ts, length(stations) == 0}
          )

        {current, []}
    end
  end

  def compare(former, current) do
    Map.reject(former, fn {k, v} -> Map.fetch!(current, k) == v end)
  end
end

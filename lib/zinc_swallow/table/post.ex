defmodule ZincSwallow.Table.Post do
  @table_name :zincswallow_post

  def create_table() do
    :mnesia.create_table(@table_name,
      disc_copies: [node()],
      attributes: [:id, :platform, :station_message_id, :version, :darwin_ts],
      index: [:platform, :station_message_id, :version, :darwin_ts]
    )
  end

  def insert(%{
        post_id: post_id,
        platform: platform,
        station_message_id: station_message_id,
        version: version,
        darwin_ts: darwin_ts
      }) do
    :mnesia.write(
      {@table_name, {platform, post_id}, platform, station_message_id, version, darwin_ts}
    )
  end

  def get(%{station_message_id: station_message_id}) do
    :mnesia.match_object({@table_name, :_, :_, station_message_id, :_, :_})
    |> Enum.map(fn {@table_name, {platform, post_id}, platform, station_message_id, version,
                    darwin_ts} ->
      %{
        post_id: post_id,
        platform: platform,
        station_message_id: station_message_id,
        version: version,
        darwin_ts: darwin_ts
      }
    end)
  end
end

defmodule ZincSwallow.Util.Processing.XmlTools do
  def transform_xml(record, record_map) when is_tuple(record) do
    record_id = elem(record, 0)
    record = Tuple.to_list(record)
    record_names = Map.get(record_map, record_id)

    Map.new(
      Enum.map(Enum.zip([:_id] ++ record_names, record), fn {key, value} ->
        {key, transform_xml(value, record_map)}
      end)
    )
  end

  def transform_xml(record, record_map) when is_list(record) do
    if Enum.all?(record, &Kernel.is_integer/1) do
      List.to_string(record)
    else
      Enum.map(record, fn element -> transform_xml(element, record_map) end)
    end
  end

  def transform_xml(:undefined, _record_map) do
    nil
  end

  def transform_xml(record, _record_map) do
    record
  end

  def preprocess_ow_xml(message_body) do
    message_body
    |> String.replace("\n", "")
    |> String.replace(~r[<ns7:Msg>(.+)</ns7:Msg>]s, fn match ->
      ~c"<ns7:Msg>" ++
        :xmerl_lib.export_text(String.replace(match, ~r"</?ns7:Msg>", "")) ++ ~c"</ns7:Msg>"
    end)
  end

  def darwin_to_markdown([record | _]) do
    record
    |> String.replace(~r{<ns7:a href="(.*)">(.*)</ns7:a>}, "[\\2](\\1)")
    |> String.replace("</ns7:p>", "\n")
    |> String.replace("<ns7:p>", "")
    |> String.replace("<ns7:p/>", "")
  end

  def markdown_to_telegram(nil), do: nil

  def markdown_to_telegram(record) do
    record
    |> String.replace(".", "\\.")
    |> String.replace("-", "\\-")
    |> String.replace("+", "\\+")
  end
end

defmodule ZincSwallow.Util.Processing.TelegramFormat do
  @escape_substitutes [
    "\\",
    "_",
    "*",
    "[",
    "]",
    "(",
    ")",
    "~",
    "`",
    ">",
    "#",
    "+",
    "-",
    "=",
    "|",
    "{",
    "}",
    ".",
    "!"
  ]
  @escape_substitutes_pre_xml ["\\", "[", "]", "(", ")"]
  @escape_substitutes_post_xml [
    "_",
    "*",
    "~",
    "`",
    ">",
    "#",
    "+",
    "-",
    "=",
    "|",
    "{",
    "}",
    ".",
    "!"
  ]

  defp subst(text, substitutes) do
    Enum.reduce(substitutes, text, fn x, acc -> String.replace(acc, x, "\\" <> x) end)
  end

  def format({:link, href, a}) do
    "[#{a}](#{href})"
  end

  def format({:text, text}) do
    text |> subst(@escape_substitutes)
  end

  def format({:xml, [text | _]}) do
    format({:xml, text})
  end

  def format({:xml, text}) do
    text
    |> subst(@escape_substitutes_pre_xml)
    |> String.replace("&amp;", "&")
    |> String.replace(~r{<ns7:a href="(.*)">(.*)</ns7:a>}, "[\\2](\\1)")
    |> String.replace("</ns7:p>", "\n")
    |> String.replace("<ns7:p>", "")
    |> String.replace("<ns7:p/>", "")
    |> subst(@escape_substitutes_post_xml)
  end

  def format({:stations, []}) do
    "_no stations_"
  end

  def format({:stations, stations}) when length(stations) > 99 do
    format({:stations, Enum.slice(stations, 0, 99)}) <>
      ", … \\(truncated, full list has #{length(stations)} entries\\)"
  end

  def format({:stations, stations}) do
    stations
    |> Enum.map(fn crs ->
      "[#{crs}](https://www.nationalrail.co.uk/live-trains/departures/#{crs}/)"
    end)
    |> Enum.join(", ")
  end

  def format(elements) when is_list(elements) do
    elements |> Enum.map(&format/1) |> Enum.join()
  end

  def join(fragments, join_element) do
    Enum.reduce(fragments, [], fn
      x, [] -> x
      x, acc -> acc ++ [{:text, "\n"}] ++ x
    end)
  end
end

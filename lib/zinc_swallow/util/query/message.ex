defmodule ZincSwallow.Util.Query.Message do
  import Ecto.Query
  alias ZincSwallow.Repo
  alias ZincSwallow.Schema.{StationMessage}

  def get_message_by_id(id) do
    Repo.one(
      from(message in StationMessage,
        where: message.id == ^id
      )
    )
  end

  defp diff({key, first}, {_key, second}) when is_list(first) do
    {key, {first -- second, second -- first}}
  end

  defp diff({key, first}, {_key, second}), do: {key, {first, second}}

  defp extract_summary(nil), do: nil

  defp extract_summary({:ok, record}), do: extract_summary(record)

  defp extract_summary(record) do
    %{message: record.message, stations: record.stations, severity: record.severity}
  end

  defp extract_changes(nil, _new_record = %{stations: []}) do
    {:delete, nil, nil, %{}}
  end

  defp extract_changes(nil, new_record) do
    {:create, nil, new_record, %{}}
  end

  defp extract_changes(old_record, new_record) when old_record == new_record do
    {:nochange, old_record, new_record, %{}}
  end

  defp extract_changes(old_record, _new_record = %{stations: []}) do
    {:delete, old_record, nil, %{}}
  end

  defp extract_changes(old_record, new_record) do
    {:modify, old_record, new_record,
     Map.new(
       Enum.map(Enum.zip(old_record, new_record), fn {first, second} -> diff(first, second) end)
       |> Enum.filter(fn {_key, {first, second}} -> first != second end)
     )}
  end

  def put_and_compare_message(
        id,
        _ = %{
          message: message,
          stations: stations,
          category: category,
          severity: severity,
          suppress: suppress
        },
        last_feed_message
      ) do
    original_row = get_message_by_id(id)

    active = length(stations) != 0
    time_now = DateTime.utc_now()

    {:ok, new_row} =
      Repo.insert(
        StationMessage.changeset_full(%StationMessage{}, %{
          id: id,
          message: message,
          stations: stations,
          category: category,
          suppress: suppress,
          severity: severity,
          last_feed_message: last_feed_message,
          active: active,
          inserted_at: time_now,
          updated_at: time_now
        }),
        on_conflict: {:replace_all_except, [:inserted_at, :telegram_last_message_id]},
        conflict_target: :id,
        returning: true
      )

    {:ok, id, extract_changes(extract_summary(original_row), extract_summary(new_row)),
     new_row.telegram_last_message_id}
  end

  def update_telegram_last_message_id(id, telegram_last_message_id) do
    time_now = DateTime.utc_now()

    {:ok, _new_row} =
      Repo.update(
        StationMessage.changeset_telegram(%StationMessage{id: id}, %{
          id: id,
          updated_at: time_now,
          telegram_last_message_id: telegram_last_message_id
        })
      )
  end
end

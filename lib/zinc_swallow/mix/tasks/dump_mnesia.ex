defmodule Mix.Tasks.ZincSwallow.DumpMnesia do
  @moduledoc "Migrate legacy postgres database to mnesia"
  use Mix.Task

  alias ZincSwallow.Struct
  alias ZincSwallow.Table

  def start_applications do
    Application.ensure_all_started(:mnesia)
  end

  def stop_applications do
    :init.stop()
  end

  def run(_) do
    start_applications()

    :mnesia.wait_for_tables([:zincswallow_message_changelog], :infinity)

    {:atomic, records} = :mnesia.transaction(&dump/0)
    IO.inspect(records)

    stop_applications()
  end

  def dump() do
    :mnesia.match_object({:zincswallow_message_changelog, :_, :_, :_, :_, :_})
  end
end

defmodule Mix.Tasks.ZincSwallow.MigrateToMnesia do
  @moduledoc "Migrate legacy postgres database to mnesia"
  use Mix.Task

  alias Mix.EctoSQL
  alias Mix.Ecto

  alias ZincSwallow.Repo
  alias ZincSwallow.Schema
  alias ZincSwallow.Struct
  alias ZincSwallow.Table

  @apps [
    :postgrex,
    :ecto,
    :ecto_sql,
    :mnesia
  ]

  def start_applications do
    Enum.each(@apps, fn app -> {:ok, _} = Application.ensure_all_started(app) end)

    repos = Ecto.parse_repo([])

    Enum.each(repos, fn repo ->
      Ecto.ensure_repo(repo, [])
      {:ok, _pid} = Repo.start_link()
    end)
  end

  def stop_applications do
    :init.stop()
  end

  def run(_) do
    start_applications()

    :mnesia.wait_for_tables([:zincswallow_message_changelog], :infinity)

    {:atomic, _} = :mnesia.transaction(&migrate/0)

    stop_applications()
  end

  defp gbnr_strip(text) do
    text |> String.replace("gb-nr:", "")
  end

  def migrate() do
    station_messages = Repo.all(Schema.StationMessage)

    station_messages
    |> Enum.map(fn %Schema.StationMessage{
                     id: station_message_id,
                     severity: station_message_severity,
                     category: station_message_category,
                     suppress: station_message_suppress,
                     stations: station_message_stations,
                     message: station_message_message,
                     active: _station_message_active,
                     last_feed_message: station_message_darwin_ts,
                     telegram_last_message_id: _telegram_last_message_id,
                     inserted_at: _,
                     updated_at: _
                   } ->
      %Struct.StationMessage{
        id: gbnr_strip(station_message_id),
        severity: station_message_severity,
        category: station_message_category,
        suppress: station_message_suppress,
        stations: Enum.map(station_message_stations, &gbnr_strip/1),
        message: station_message_message,
        darwin_ts: station_message_darwin_ts
      }
    end)
    |> IO.inspect()
    |> Enum.map(
      fn current = %Struct.StationMessage{id: id, darwin_ts: darwin_ts, stations: stations} ->
        %{
          id: id,
          current: current,
          changelog: [],
          darwin_ts: darwin_ts,
          is_cleared: length(stations) == 0
        }
      end
    )
    |> Enum.map(&Table.StationMessage.insert/1)

    station_messages
    |> Enum.map(fn %Schema.StationMessage{
                     id: station_message_id,
                     telegram_last_message_id: telegram_last_message_id,
                     last_feed_message: darwin_ts
                   } ->
      %{
        platform: :telegram,
        station_message_id: gbnr_strip(station_message_id),
        post_id: telegram_last_message_id,
        version: 0,
        darwin_ts: darwin_ts
      }
    end)
    |> Enum.map(&Table.Post.insert/1)
  end
end

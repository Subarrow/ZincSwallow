defmodule Mix.Tasks.ZincSwallow.InitMnesia do
  @moduledoc "Create mnesia tables"
  use Mix.Task

  alias ZincSwallow.Table.{Post, StationMessage}

  def run(_) do
    :ok = :mnesia.create_schema([])
    {:ok, [:mnesia]} = Application.ensure_all_started(:mnesia)

    {:atomic, :ok} = StationMessage.create_table()
    {:atomic, :ok} = Post.create_table()

    :ok = :init.stop()
  end
end

defmodule ZincSwallow.Application do
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    schema_record_definitions = Application.fetch_env!(:zinc_swallow, :schema_record_defs)

    {:ok, schema_model} =
      :erlsom.compile_xsd_file(~c"priv/darwin_v16_schema/rttiPPTSchema_v16.xsd", [
        {:include_dirs, [~c"priv/darwin_v16_schema"]}
      ])

    relay_host = String.to_charlist(Application.fetch_env!(:zinc_swallow, :relay_host))
    relay_port = Application.fetch_env!(:zinc_swallow, :relay_port)
    relay_username = Application.fetch_env!(:zinc_swallow, :relay_username)
    relay_password = Application.fetch_env!(:zinc_swallow, :relay_password)
    relay_queue = Application.fetch_env!(:zinc_swallow, :relay_queue)
    target_channel = Application.fetch_env!(:zinc_swallow, :target_channel)

    subscribe_params =
      case Application.fetch_env!(:zinc_swallow, :stream_mode) do
        :first -> [{"x-stream-offset", "first"}]
        :last -> [{"x-stream-offset", "last"}]
      end

    children = [
      ZincSwallow.Repo,
      {ZincSwallow.BackoffManager, opts: %{}},
      {ZincSwallow.Distribute, %{}},
      {ZincSwallow.Store, %{}},
      {ZincSwallow.Parser,
       %{
         schema_record_definitions: schema_record_definitions,
         schema_model: schema_model
       }},
      Supervisor.child_spec(
        {ZincSwallow.DarwinRelay,
         %{
           host: relay_host,
           port: relay_port,
           user: relay_username,
           pass: relay_password,
           queue: relay_queue,
           id: "darwin_intake",
           target_channel: target_channel,
           subscribe_params: subscribe_params
         }},
        id: :darwin_input
      )
    ]

    opts = [strategy: :one_for_one, name: ZincSwallow.Supervisor]
    Supervisor.start_link(children, opts)
  end
end

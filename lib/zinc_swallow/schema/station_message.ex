defmodule ZincSwallow.Schema.StationMessage do
  use Ecto.Schema
  import Ecto.Changeset
  @primary_key false

  schema "zincswallow_station_messages" do
    field(:id, :string, primary_key: true)
    field(:severity, :integer)
    field(:category, :string)
    field(:suppress, :boolean)
    field(:stations, {:array, :string})
    field(:message, :string)
    field(:active, :boolean)

    field(:last_feed_message, :utc_datetime)
    field(:telegram_last_message_id, :integer)

    field(:inserted_at, :utc_datetime)
    field(:updated_at, :utc_datetime)
  end

  def changeset_full(message, params) do
    message
    |> cast(params, [
      :id,
      :severity,
      :category,
      :suppress,
      :stations,
      :message,
      :active,
      :last_feed_message,
      :inserted_at,
      :updated_at
    ])
    |> validate_required([
      :severity,
      :category,
      :suppress,
      :stations,
      :message,
      :active,
      :last_feed_message
    ])
  end

  def changeset_telegram(message, params) do
    message
    |> cast(params, [:id, :telegram_last_message_id, :updated_at])
    |> validate_required([:id, :telegram_last_message_id, :updated_at])
  end
end

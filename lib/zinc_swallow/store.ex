defmodule ZincSwallow.Store do
  use GenServer

  require Logger

  alias ZincSwallow.Struct
  alias ZincSwallow.Table

  def start_link(_) do
    GenServer.start_link(__MODULE__, nil, name: __MODULE__)
  end

  def init(_) do
    {:ok, nil}
  end

  def handle_cast(
        station_message = %Struct.StationMessage{id: message_id},
        state
      ) do
    case :mnesia.transaction(fn ->
           Table.StationMessage.insert_and_compare(station_message)
         end) do
      # Nothing changed
      {:atomic, nil} ->
        Logger.info(
          "Discarded message ID #{message_id} because an identical (or newer) copy already exists"
        )

        nil

      # Something changed
      {:atomic, change} ->
        GenServer.cast(ZincSwallow.Distribute, change)
    end

    {:noreply, state}
  end
end

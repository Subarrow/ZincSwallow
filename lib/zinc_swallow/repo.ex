defmodule ZincSwallow.Repo do
  use Ecto.Repo,
    otp_app: :zinc_swallow,
    adapter: Ecto.Adapters.Postgres
end

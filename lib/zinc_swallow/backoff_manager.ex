defmodule ZincSwallow.BackoffManager do
  use GenServer
  require Logger

  # 10min ceiling
  @backoff_limit_millis 600_000
  # 1.5s
  @minimum_reconnect_interval 1_500

  # 10min
  @rapid_reconnect_penalty 600_000

  @rate_decay_interval 500
  @rate_decay_amount 500

  @connect_rate_cost 8000
  @rate_decay_penalty_threshold 10_000

  def start_link(opts) do
    GenServer.start_link(__MODULE__, opts, name: __MODULE__)
  end

  def init(_) do
    Process.send_after(self(), :rate_decay, @rate_decay_interval)
    {:ok, %{}}
  end

  defp ensure_member(state, connection_id) do
    Map.put_new(state, connection_id, {0, 0})
  end

  defp determine_penalty(_, rate) do
    cond do
      rate > @rate_decay_penalty_threshold -> @rapid_reconnect_penalty
      true -> 0
    end
  end

  def handle_info(:rate_decay, state) do
    Process.send_after(self(), :rate_decay, @rate_decay_interval)

    {:noreply,
     Enum.map(state, fn {name, {interval, rate}} ->
       {name, {interval, Kernel.max(rate - @rate_decay_amount, 0)}}
     end)
     |> Map.new()}
  end

  def handle_call({:get_interval, connection_id}, _from, state) do
    {interval, rate} = Map.get(state, connection_id, {0, 0})
    interval = Kernel.max(interval, @minimum_reconnect_interval)
    {:reply, {interval, rate}, ensure_member(state, connection_id)}
  end

  def handle_call({:connect_failed, connection_id}, _from, state) do
    {:reply, :ok,
     Map.update(state, connection_id, 0, fn {interval, rate} ->
       {Kernel.min(Kernel.max(interval, 2000) * 2, @backoff_limit_millis), rate}
     end)}
  end

  def handle_call({:connect_success, connection_id}, _from, state) do
    {:reply, :ok, Map.update(state, connection_id, {0, 0}, fn {_, rate} -> {0, rate} end)}
  end

  def handle_call({:connect, connection_id}, _from, state) do
    {:reply, :ok,
     Map.update(state, connection_id, {0, 0}, fn {interval, rate} ->
       {interval + determine_penalty(interval, rate), rate + @connect_rate_cost}
     end)}
  end

  def get_interval(connection_id) do
    GenServer.call(__MODULE__, {:get_interval, connection_id})
  end

  def notify_failure(connection_id) do
    GenServer.call(__MODULE__, {:connect_failed, connection_id})
  end

  def notify_success(connection_id) do
    GenServer.call(__MODULE__, {:connect_success, connection_id})
  end

  def notify_connect(connection_id) do
    GenServer.call(__MODULE__, {:connect, connection_id})
  end
end

defmodule ZincSwallow.Parser do
  use GenServer

  require Logger

  alias Barytherium.Frame

  alias ZincSwallow.Util.Processing.XmlTools
  alias ZincSwallow.Struct

  def start_link(state) do
    GenServer.start_link(__MODULE__, state, name: __MODULE__)
  end

  def init(
        state = %{
          schema_record_definitions: _schema_record_definitions,
          schema_model: _schema_model
        }
      ) do
    {:ok, state}
  end

  defp extract_ts(%Frame{headers: headers}) do
    timestamp_raw = headers |> Frame.headers_to_map() |> Map.fetch!("timestamp")
    {:ok, timestamp} = DateTime.from_unix(trunc(elem(Integer.parse(timestamp_raw), 0) / 1000))

    timestamp
  end

  defp parse_xml(%Frame{body: body}, %{
         schema_record_definitions: schema_record_definitions,
         schema_model: schema_model
       }) do
    body_escaped = XmlTools.preprocess_ow_xml(body)

    {:ok, erlsom_out, _rest} =
      :erlsom.scan(body_escaped, schema_model, [{:expand_entities, true}])

    XmlTools.transform_xml(erlsom_out, schema_record_definitions)
  end

  def handle_cast(
        frame = %Frame{command: :message, body: _body, headers: _headers},
        state
      ) do
    _timestamp = extract_ts(frame)

    parsed_station_message =
      %Struct.StationMessage{message: message_texts, id: message_id, stations: message_stations} =
      parse_xml(frame, state) |> Struct.StationMessage.from_root_xml()

    Logger.info(
      "Received message ID #{message_id}: #{inspect(message_texts)}, with #{length(message_stations)} stations"
    )

    GenServer.cast(ZincSwallow.Store, parsed_station_message)

    {:noreply, state}
  end
end

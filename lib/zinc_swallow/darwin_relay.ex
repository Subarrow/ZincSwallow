defmodule ZincSwallow.DarwinRelay do
  use GenServer
  alias Barytherium.Frame
  alias Barytherium.Network
  alias Barytherium.Network.Sender
  alias ZincSwallow.BackoffManager
  require Logger

  defp decompress_frame_body(message = %Frame{body: body}) do
    %Frame{message | body: :zlib.gunzip(:binary.bin_to_list(body))}
  end

  def start_link(opts) do
    GenServer.start_link(__MODULE__, %{opts: opts}, name: __MODULE__)
  end

  def init(state = %{opts: %{id: id}}) do
    BackoffManager.notify_connect(id)
    {interval, _} = BackoffManager.get_interval(id)
    Logger.info("#{id} waiting before attempting (re)connection #{interval}ms")
    Process.send_after(self(), :connect, interval)
    {:ok, state}
  end

  def handle_info(:connect, state = %{opts: %{host: host, port: port}}) do
    Network.start_link(self(), host, port, [])
    {:noreply, state}
  end

  def handle_cast(
        {:barytherium, :frames, {[message = %Frame{command: :connected}], sender_pid}},
        state = %{opts: %{id: id, queue: queue, subscribe_params: subscribe_params}}
      ) do
    Logger.info("Received connected frame: #{inspect(message)}")
    BackoffManager.notify_success(id)

    Sender.write(
      sender_pid,
      [
        %Barytherium.Frame{
          command: :subscribe,
          headers: [{"id", "0"}, {"destination", queue}, {"ack", "client"}] ++ subscribe_params
        }
      ]
    )

    {:noreply, state}
  end

  def handle_cast(
        {:barytherium, :frames, {frames, sender_pid}},
        state
      ) do
    Enum.map(frames, &decompress_frame_body/1)
    |> Enum.each(&GenServer.cast(ZincSwallow.Parser, &1))

    %Frame{headers: last_frame_headers} = List.last(frames)
    %{"ack" => ack_id} = Frame.headers_to_map(last_frame_headers)

    Sender.write(sender_pid, [%Frame{command: :ack, headers: [{"id", ack_id}]}])

    {:noreply, state}
  end

  def handle_cast(
        {:barytherium, :connect, {:ok, sender_pid}},
        state = %{opts: %{user: user, pass: pass}}
      ) do
    Logger.info("Connection succeeded, remote end has picked up")

    Sender.write(sender_pid, [
      %Frame{
        command: :connect,
        headers: [{"accept-version", "1.2"}, {"host", "/"}, {"login", user}, {"passcode", pass}]
      }
    ])

    {:noreply, state}
  end

  def handle_cast({:barytherium, :disconnect, reason}, state) do
    Logger.error("Connection disconnected for reason #{reason}")
    {:stop, :connect_disconnected, state}
  end

  def handle_cast({:barytherium, :connect, {:error, error}}, state = %{opts: %{id: id}}) do
    Logger.error("Connection failed, error: #{error}")
    BackoffManager.notify_failure(id)
    {:stop, :connect_failed, state}
  end
end

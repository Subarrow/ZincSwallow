defmodule ZincSwallow.Distribute do
  use GenServer

  require Logger

  alias ZincSwallow.Util.Processing.TelegramFormat
  alias ZincSwallow.Struct
  alias ZincSwallow.Table

  def start_link(_) do
    GenServer.start_link(__MODULE__, nil, name: __MODULE__)
  end

  def init(_) do
    {:ok, nil}
  end

  def changelog_summary([]) do
    "no previous changes"
  end

  def changelog_summary(changelog) when length(changelog) == 1 do
    "1 previous change"
  end

  def changelog_summary(changelog) do
    "#{length(changelog)} previous changes"
  end

  def handle_cast(
        {%Struct.StationMessage{
           id: station_message_id,
           category: category,
           severity: severity,
           message: message,
           stations: stations,
           darwin_ts: darwin_ts
         }, changelog},
        state
      ) do
    formatted_message =
      [
        {:link, "https://mercury.kanaya.dev/darwin/station_message/#{station_message_id}",
         station_message_id},
        {:text, " (#{category}, #{severity})"},
        {:text, "\n"},
        {:xml, message},
        {:text, "\n\n"},
        {:stations, stations},
        {:text, "\n(#{changelog_summary(changelog)})"}
      ]

    case :mnesia.transaction(fn -> Table.Post.get(%{station_message_id: station_message_id}) end) do
      {:atomic, [%{post_id: existing_post_id}]} ->
        # EDIT POST
        Logger.info(
          "Editing existing post #{existing_post_id} for station message #{station_message_id}:\n#{inspect(formatted_message)}"
        )

        edit_message(formatted_message, existing_post_id)

      # NEW POST
      {:atomic, []} ->
        Logger.info(
          "New post for station message #{station_message_id}:\n#{inspect(formatted_message)}"
        )

        case send_message(formatted_message) do
          {:ok, telegram_message_id} ->
            {:atomic, _} =
              :mnesia.transaction(fn ->
                Table.Post.insert(%{
                  post_id: telegram_message_id,
                  platform: :telegram,
                  station_message_id: station_message_id,
                  version: 1,
                  darwin_ts: darwin_ts
                })
              end)

          {:error, rationale} ->
            Logger.info("some sort of error sending telegram message: #{inspect(rationale)}")
        end
    end

    {:noreply, state}
  end

  defp send_message(formatted_message) do
    token = Application.fetch_env!(:zinc_swallow, :telegram_token)
    chan_id = Application.fetch_env!(:zinc_swallow, :target_channel)

    headers = [{"Content-Type", "application/json"}]

    payload =
      %{
        "chat_id" => chan_id,
        "parse_mode" => "MarkdownV2",
        "disable_web_page_preview" => true,
        "text" => TelegramFormat.format(formatted_message)
      }
      |> Jason.encode!()

    case :hackney.post("https://api.telegram.org/bot#{token}/sendMessage", headers, payload, [
           :with_body
         ]) do
      {:ok, 200, _headers, body} ->
        {:ok, %{"result" => %{"message_id" => telegram_message_id}}} = Jason.decode(body)
        {:ok, telegram_message_id}

      {:ok, _, _headers, body} ->
        Logger.error("Non-200 response from Telegram: #{body}")
        {:error, body}

      {:error, hackney_error} ->
        Logger.error("Couldn't send Telegram message: #{hackney_error}")
        {:error, hackney_error}
    end
  end

  defp edit_message(formatted_message, message_id) do
    token = Application.fetch_env!(:zinc_swallow, :telegram_token)
    chan_id = Application.fetch_env!(:zinc_swallow, :target_channel)

    headers = [{"Content-Type", "application/json"}]

    payload =
      %{
        "chat_id" => chan_id,
        "message_id" => message_id,
        "parse_mode" => "MarkdownV2",
        "disable_web_page_preview" => true,
        "text" => TelegramFormat.format(formatted_message)
      }
      |> Jason.encode!()

    case :hackney.post("https://api.telegram.org/bot#{token}/editMessageText", headers, payload, [
           :with_body
         ]) do
      {:ok, 200, _headers, body} ->
        {:ok, %{"result" => %{"message_id" => telegram_message_id}}} = Jason.decode(body)
        {:ok, telegram_message_id}

      {:ok, _, _headers, body} ->
        Logger.error("Non-200 response from Telegram: #{body}")
        {:error, body}

      {:error, hackney_error} ->
        Logger.error("Couldn't send Telegram message: #{hackney_error}")
        {:error, hackney_error}
    end
  end
end

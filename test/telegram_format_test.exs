defmodule TelegramFormatTest do
  use ExUnit.Case

  alias ZincSwallow.Util.Processing.TelegramFormat
  doctest ZincSwallow.Util.Processing.TelegramFormat

  test "truncates stations" do
    str = TelegramFormat.format({:stations, Enum.map(1..101, fn _ -> "AAA" end)})

    assert str ==
             "AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, AAA, … \\(truncated, full list has 101 entries\\)"
  end
end

# zincSwallow

This project relays summaries of OW-type messages on the Darwin push port to a
Telegram channel, with a local cache held in a table in postgres.

I plan to support targets other than telegram (preference towards more open
platforms, such as IRC)

## Setup

### DarwinShovel
This is intended to be used with
[DarwinShovel](https://codeberg.org/Subarrow/DarwinShovel), since it assumes
only OW messages will be delivered to it. The example config includes an
example on how to do this.

You can't attach this bot directly to the push port without editing it to discard non-OW
messages, and similarly, you can't use this with the ActiveMQ shovel method referenced
on the wiki.

### Telegram
Use the [@botfather](https://telegram.me/botfather) bot to create a bot and
get its access token.

Make a channel to use as the target, be sure to invite the bot to the channel,
and make sure that the channel is public, Nadia seemingly can't send messages
to channels which don't have a @.

### Config file

touch config/prod.secret.exs, with the following contents:

```
import Config

config :zinc_swallow,
  relay_host: "example.com",
  relay_port: 61613,
  relay_username: "arsenicCatnip",
  relay_password: "hunter2",
  relay_queue: "/amq/queue/darwin-v16-xml-ow",
  target_channel: "@zincswallow_test",
  telegram_token: "0000000000:AAAAaaaaAaaaaaAaAAAAAaaaaAaaaaaaaaa",
  stream_mode: :last

config :zinc_swallow, ZincSwallow.Repo,
  database: "zincswallow_test",
  username: "user",
  password: "",
  socket_dir: "/var/run/postgresql"

config :zinc_swallow, ecto_repos: [ZincSwallow.Repo]
```

Migrating to mnesia:
```
mix ZincSwallow.InitMnesia
mix ZincSwallow.MigrateToMnesia
```

Edit as appropriate

Run migrations to create/update tables: `mix ecto.migrate`

To run, in most cases, `MIX_ENV=prod mix run --no-halt` should be fine. You
can also run `MIX_ENV=prod mix release zinc_swallow` if you *particularly*
need a release binary (if you don't know what this means, you probably
don't).

defmodule ZincSwallow.MixProject do
  use Mix.Project

  def project do
    [
      app: :zinc_swallow,
      version: "0.1.0",
      elixir: "~> 1.13",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      releases: [
        zinc_swallow: [
          version: "0.1.0",
          applications: [zinc_swallow: :permanent]
        ]
      ]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger, :xmerl, :mnesia],
      mod:
        case Mix.env() do
          :test -> []
          _ -> {ZincSwallow.Application, []}
        end
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:barytherium, "~> 0.6.2"},
      {:tzdata, "~> 1.1"},
      {:erlsom, "~> 1.5"},
      {:ecto_sql, "~> 3.0"},
      {:postgrex, ">= 0.0.0"},
      {:hackney, "~> 1.20"},
      {:jason, "~> 1.4.1"},
      {:credo, "~> 1.6", only: [:dev, :test], runtime: false}
    ]
  end
end

import Config

config :logger, :console, level: :info

config :elixir, :time_zone_database, Tzdata.TimeZoneDatabase

config :zinc_swallow,
  schema_record_defs:
    Map.new(
      Enum.map(Record.extract_all(from: "priv/darwin_v16.hrl"), fn {key, value} ->
        {key, elem(Enum.unzip(value), 0)}
      end)
    )

import_config "#{Mix.env()}.exs"

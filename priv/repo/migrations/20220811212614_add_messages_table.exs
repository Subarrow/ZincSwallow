defmodule ZincSwallow.Repo.Migrations.AddMessagesTable do
  use Ecto.Migration

  def change do
    create table("zincswallow_station_messages", primary_key: false) do
      add :id, :string, primary_key: true
      add :severity, :integer, null: false
      add :category, :text, null: false
      add :suppress, :boolean, null: false, default: false
      add :stations, {:array, :text}, null: false, default: []
      add :message, :text, null: false
      
      add :active, :boolean, null: false
      add :last_feed_message, :timestamp, null: false

      add :telegram_last_message_id, :bigint, null: true

      timestamps()
    end
  end
end
